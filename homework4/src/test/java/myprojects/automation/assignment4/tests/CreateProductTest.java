package myprojects.automation.assignment4.tests;

import myprojects.automation.assignment4.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import myprojects.automation.assignment4.GeneralActions;

public class CreateProductTest extends BaseTest {
          GeneralActions actions;
          GeneralActions name;
          GeneralActions qty;
          GeneralActions price;
          WebDriverWait wait;

    @Test
    public void createNewProduct(String login, String password) {

        actions.login(login,password);
        driver.get("http://prestashop-automation.qatestlab.com.ua/");
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();"
                ,driver.findElement(By.xpath("//a[@class='all-product-link pull-xs-left pull-md-right h4']")));
        driver.findElement(By.xpath("//a[@class='all-product-link pull-xs-left pull-md-right h4']")).click();
        driver.findElement(By.xpath("//input[@class='ui-autocomplete-input']")).sendKeys(String.valueOf(name));
        driver.findElement(By.xpath("//input[@class='ui-autocomplete-input']")).submit();

        Assert.assertEquals (driver.findElement(By.xpath("//h1[@class='h3 product-title']")).getText(),String.valueOf(name));
        Assert.assertEquals (driver.findElement(By.xpath("//div[@class='product-price-and-shipping']")).getText(),String.valueOf(price));

    }
    @Test
    public void checkVisibility (){

        driver.findElement(By.xpath("//h1[@class='h3 product-title']")).click();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='product-quantities']"))));

        Assert.assertEquals (driver.findElement(By.xpath("//div[@class='product-quantities']")).getText(),String.valueOf(qty));
            System.out.println(driver.findElement(By.xpath("//div[@class='product-quantities']")).getText());
        Assert.assertEquals (driver.findElement(By.xpath("//h1[@itemprop='name']")).getText(),String.valueOf(name));
            System.out.println(driver.findElement(By.xpath("//h1[@itemprop='name']")).getText());
        Assert.assertEquals (driver.findElement(By.xpath("//span[@itemprop='price']")).getText(),String.valueOf(price));
            System.out.println(driver.findElement(By.xpath("//span[@itemprop='price']")).getText());
    }

}
