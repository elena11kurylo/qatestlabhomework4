package myprojects.automation.assignment4;


import myprojects.automation.assignment4.model.ProductData;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import myprojects.automation.assignment4.utils.Properties;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;
    public String login ="webinar.test@gmail.com";
    public String password ="Xcg7299bnSmMuRLp9ITw";
    ProductData newProduct= ProductData.generate();


    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        driver.get(Properties.getBaseAdminUrl());
        for (int i=0; i<=3; i++) {
            try{
                driver.findElement(By.id("email")).sendKeys(login);
                driver.findElement(By.id("passwd")).sendKeys(password);
                driver.findElement(By.name("submitLogin")).click();
                waitForContentLoad();
            }
            catch( NoSuchElementException e){}
        }

        throw new UnsupportedOperationException();


    }

    public void createProduct(ProductData newProduct) {
        String name = newProduct.getName();
        int qty = newProduct.getQty();
        String price = newProduct.getPrice();



        (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//nav[@id='nav-sidebar']")));
        Actions builder = new Actions(driver);
        builder.moveToElement(driver.findElement(By.id("subtab-AdminCatalog"))).build().perform();
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.id("subtab-AdminCategories")));
        driver.findElement(By.xpath("//li[@data-submenu='10']")).click();

        driver.findElement(By.xpath("//a[@data-placement='bottom']")).click();
        driver.findElement(By.id("form_step1_name_1")).sendKeys(name);
        driver.findElement(By.id("form_step1_qty_0_shortcut")).clear();
        driver.findElement(By.id("form_step1_qty_0_shortcut")).sendKeys( String.valueOf(qty));
        driver.findElement(By.id("form_step1_price_ttc_shortcut")).clear();
        driver.findElement(By.id("form_step1_price_ttc_shortcut")).sendKeys(price);
        driver.findElement(By.className("switch-input")).click();

            if (driver.findElement(By.xpath("//input[@id = 'form_step1_active']")).isDisplayed());
                {System.out.println("Product is activated");}

        driver.findElement(By.xpath("//button[@type='submit']"));

            if (driver.findElement(By.xpath("//input[@id = 'form_step1_active']")).isDisplayed());
                {System.out.println("Product is activated");}

        driver.get("http://prestashop-automation.qatestlab.com.ua/");
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();"
                ,driver.findElement(By.xpath("//a[@class='all-product-link pull-xs-left pull-md-right h4']")));
        driver.findElement(By.xpath("//a[@class='all-product-link pull-xs-left pull-md-right h4']")).click();
        driver.findElement(By.xpath("//input[@class='ui-autocomplete-input']")).sendKeys(name);
        driver.findElement(By.xpath("//input[@class='ui-autocomplete-input']")).submit();

        throw new UnsupportedOperationException();
    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad() {

        wait.until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));

    }
}
